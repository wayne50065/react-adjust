import React from "react";

export function useAdjust(props) {
  const [widths, setWidths] = React.useState({});
  const [heights, setHeights] = React.useState({});
  const [containerDimensions, setContainerDimensions] = React.useState({});
  const [expand, setExpand] = React.useState(false);
  const dragEdgeStyle = {
    vertical: {
      position: "absolute",
      height: "10px",
      top: "0",
      width: "100%",
      zIndex: 10000,
      background: "transparent",
    },
    horizontal: {
      // position: 'absolute',
      width: "10px",
      // top: '0',
      height: "100%",
      zIndex: 10000,
      background: "transparent",
    },
  };
  function UpdateHeight(target, displacement, topLimit, bottomLimit) {
    const resizeContainer = document.querySelector(`#${target}`);
    const containerHeight = resizeContainer.offsetHeight;
    const newHeight_percent =
      heights[target].upper - (displacement / containerHeight) * 100;
    const topLimit_percent = (topLimit / containerHeight) * 100;
    const bottomLimit_percent =
      ((containerHeight - bottomLimit) / containerHeight) * 100;

    switch (true) {
      case newHeight_percent < topLimit_percent:
        return setHeights({
          ...heights,
          [target]: {
            upper: topLimit_percent,
            lower: 100 - topLimit_percent,
          },
        });
      case newHeight_percent > bottomLimit_percent:
        return setHeights({
          ...heights,
          [target]: {
            upper: bottomLimit_percent,
            lower: 100 - bottomLimit_percent,
          },
        });
      default:
        return setHeights({
          ...heights,
          [target]: {
            upper: newHeight_percent,
            lower: 100 - newHeight_percent,
          },
        });
    }
  }
  function UpdateWidth(target, displacement, leftLimit, rightLimit) {
    const resizeContainer = document.querySelector(`#${target}`);
    const containerWidth = resizeContainer.offsetWidth;
    const newWidth_percent =
      widths[target].left - (displacement / containerWidth) * 100;
    const leftLimit_percent = (leftLimit / containerWidth) * 100;
    const rightLimit_percent =
      ((containerWidth - rightLimit) / containerWidth) * 100;

    switch (true) {
      case newWidth_percent < leftLimit_percent:
        return setWidths({
          ...widths,
          [target]: {
            left: leftLimit_percent,
            right: 100 - leftLimit_percent,
          },
        });
      case newWidth_percent > rightLimit_percent:
        return setWidths({
          ...widths,
          [target]: {
            left: rightLimit_percent,
            right: 100 - rightLimit_percent,
          },
        });
      default:
        return setWidths({
          ...widths,
          [target]: {
            left: newWidth_percent,
            right: 100 - newWidth_percent,
          },
        });
    }
  }

  function OnDragStart(e, direction, container_target, limits) {
    //get the dimensions of resize container
    const resizeContainer = document.querySelector(`#${container_target}`);
    setContainerDimensions({
      ...containerDimensions,
      [container_target]: {
        width: resizeContainer.offsetWidth,
        height: resizeContainer.offsetHeight,
      },
    });
    //record start position
    const startX = e.clientX;
    const startY = e.clientY;
    //listening to drag actions and update the end position of the resize edge
    document.body.onmousemove = (BodyEvent) => {
      if (direction === "vertical") {
        const displacement_Y = startY - BodyEvent.clientY;
        UpdateHeight(
          container_target,
          displacement_Y,
          limits.top,
          limits.bottom
        );
      }
      if (direction === "horizontal") {
        const displacement_X = startX - BodyEvent.clientX;
        UpdateWidth(
          container_target,
          displacement_X,
          limits.left,
          limits.right
        );
      }
    };
    // clear listener when mouse up
    document.body.onmouseup = () => {
      document.body.onmousemove = null;
    };
  }
  function VerticalResizeEdge(props) {
    // update layout when window is resized
    React.useEffect(() => {
      const onWindowResize = () => {
        UpdateHeight(props.target, 0, props.limits.top, props.limits.bottom);
      };
      window.addEventListener("resize", onWindowResize);
      return () => {
        window.removeEventListener("resize", onWindowResize);
      };
    }, []);
    // set initHeights for resizable boxes
    React.useEffect(() => {
      const parent = document.querySelector(`#${props.target}`);
      if (parent.style.display === "none") return;

      if (!heights[`${props.target}`]) {
        const initHeight = parseInt(props.initHeight.slice(0, -1));
        setHeights({
          [props.target]: { upper: initHeight, lower: 100 - initHeight },
        });
      }
    }, []);

    return <EdgeElement vertical {...props} />;
  }
  function HorizontalResizeEdge(props) {
    // update layout when window is resized
    React.useEffect(() => {
      const onWindowResize = () => {
        UpdateWidth(props.target, 0, props.limits.left, props.limits.right);
      };
      window.addEventListener("resize", onWindowResize);
      return () => {
        window.removeEventListener("resize", onWindowResize);
      };
    }, []);
    // set initHeights for resizable boxes
    React.useEffect(() => {
      const parent = document.querySelector(`#${props.target}`);
      if (parent.style.display === "none") return;
      if (!widths[`${props.target}`]) {
        const initWidth = parseInt(props.initWidth.slice(0, -1));
        setWidths({
          [props.target]: { left: initWidth, right: 100 - initWidth },
        });
      }
    }, []);

    return <EdgeElement horizontal {...props} />;
  }
  //TODO://
  //not yet complete
  function ExpandToggle() {
    if (heights == props.min) {
      setExpand(false);
      setHeights(props.max);
    } else {
      setExpand(true);
      setHeights(props.min);
    }
  }

  // the resize bar body
  function EdgeElement(props) {
    const resizeDirection = () => {
      if (props.horizontal) return "horizontal";
      else return "vertical";
    };
    return (
      <div
        style={{ ...dragEdgeStyle[resizeDirection()], ...props.style }}
        draggable="true"
        onDragStart={(e) => {
          // console.log({ target: props.target });
          OnDragStart(e, resizeDirection(), props.target, props.limits);
        }}
        onMouseOver={(e) => {
          e.target.style.opacity = 1;
          e.target.style.background =
            resizeDirection() === "vertical"
              ? "linear-gradient(180deg, #00000020 0%, #00000010 30%, transparent 70%)"
              : "#00000020";
          e.target.style.cursor =
            resizeDirection() === "vertical" ? "row-resize" : "col-resize";
        }}
        onMouseOut={(e) => {
          e.target.style.opacity = 1;
          e.target.style.background = "transparent";
        }}
        id={props.name ? `'dragEdge'-${props.name}` : "dragEdge"}
      >
        {props.children}
      </div>
    );
  }
  return {
    VerticalResizeEdge,
    HorizontalResizeEdge,
    ExpandToggle,
    expand,
    heights,
    widths,
  };
}
